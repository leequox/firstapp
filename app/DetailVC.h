//
//  DetailVC.h
//  coredomo
//
//  Created by LeeQuox on 11/29/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXViewController.h"
@class Student;
@interface DetailVC : FXViewController
@property (nonatomic) BOOL isEdit;
@property (strong, nonatomic) IBOutlet UIDatePicker *dpDate;
@property (nonatomic, strong) Student *student;
//@property (strong, nonatomic) IBOutlet UIImageView *imgImage;
//- (IBAction)btPickImage:(id)sender;
@property(strong,nonatomic)UIImagePickerController *imagePickerController;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (weak, nonatomic) IBOutlet UIImageView *studentImage;
@property (weak, nonatomic) IBOutlet UILabel *tapToAddLabel;
- (IBAction)btPickImage:(id)sender;
@end

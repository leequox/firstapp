//
//  Student.m
//  coredomo
//
//  Created by LeeQuox on 11/30/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "Student.h"


@implementation Student

@dynamic age;
@dynamic name;
@dynamic sex;
@dynamic date;
@dynamic image;

@end

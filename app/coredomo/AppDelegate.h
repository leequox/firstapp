//
//  AppDelegate.h
//  coredomo
//
//  Created by LeeQuox on 11/29/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(strong,nonatomic)UINavigationController *navigationController;
@property(strong,nonatomic)UINavigationController *navigationController2;
@property(strong,nonatomic)UITabBarController *tabbar;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)loginSucess;

+(AppDelegate*)sharedAppDelegate;



@end


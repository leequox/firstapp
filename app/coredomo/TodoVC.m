//
//  TodoVC.m
//  coredomo
//
//  Created by LeeQuox on 12/16/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "TodoVC.h"
#import "DetailTodoVC.h"

#import <UIKit/UILocalNotification.h>

@interface TodoVC ()

@end

@implementation TodoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configNotifi];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[UIApplication sharedApplication]scheduledLocalNotifications]count];
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=@"Cell";
   
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    //Get List
    NSArray *arrayLocalNotifications=[[UIApplication sharedApplication]scheduledLocalNotifications];
    UILocalNotification *localNotification=[arrayLocalNotifications objectAtIndex:indexPath.row];
    
    //Show List
    [cell.textLabel setText:localNotification.alertBody ];
    [cell.detailTextLabel setText:[localNotification.fireDate description]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}
-(void)configNotifi
{
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(reloadTable)
                                                name:@"reloadData"
                                              object:nil];
}
-(void)reloadTable
{
    [self.tableview reloadData];
}
- (IBAction)btAdd:(id)sender {
    DetailTodoVC *vc=[[DetailTodoVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end

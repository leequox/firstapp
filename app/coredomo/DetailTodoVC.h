//
//  DetailTodoVC.h
//  coredomo
//
//  Created by LeeQuox on 12/16/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXViewController.h"
@interface DetailTodoVC : FXViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITextField *itemPicker;

@end

//
//  DetailTodoVC.m
//  coredomo
//
//  Created by LeeQuox on 12/16/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "DetailTodoVC.h"

@interface DetailTodoVC ()

@end

@implementation DetailTodoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btOk:(id)sender {
    //Get time
    NSDate *pickerDate=[_datePicker date];
    
    //Setup notification
    UILocalNotification *localNotification=[[UILocalNotification alloc]init];
    localNotification.fireDate=pickerDate;
    localNotification.alertBody=_itemPicker.text;
    localNotification.alertAction=@"Show me To do";
    localNotification.timeZone=[NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber=[[UIApplication sharedApplication]applicationIconBadgeNumber]+1;
    
    [[UIApplication sharedApplication]scheduleLocalNotification:localNotification];
    
    //ReloadData
    [[NSNotificationCenter defaultCenter]postNotificationName:@"reloadData" object:self];
    _itemPicker.delegate=self;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end

//
//  AppDelegate.m
//  coredomo
//
//  Created by LeeQuox on 11/29/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeVC.h"
#import "DetailVC.h"
#import"Student.h"
#import "LoginVC.h"
#import "FXNavigationController.h"
#import "TodoVC.h"
#import "AlarmVC.h"

@interface AppDelegate ()

@end
static __weak AppDelegate *sharedAppDelegate;

@implementation AppDelegate
+(AppDelegate *) sharedAppDelegate
{
    if (!sharedAppDelegate) {
        sharedAppDelegate=[UIApplication sharedApplication].delegate;
    }
    return sharedAppDelegate;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window=[[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    LoginVC *loginVC=[[LoginVC alloc]init];
    self.window.rootViewController=loginVC;
    [self.window makeKeyAndVisible];
    //Setup Notification
    if ([[[UIDevice currentDevice]systemVersion]floatValue]>=8.0) {
        [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert| UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication]registerForRemoteNotificationTypes:(UIUserNotificationTypeAlert| UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)];
    }
    
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Set icon badge number to zero
        application.applicationIconBadgeNumber = 0;
    }
    
    //Setup Tabbar
    HomeVC *homeVC=[[HomeVC alloc]init];
    FXNavigationController *naviHomeVC=[[FXNavigationController alloc]initWithRootViewController:homeVC];
    
    AlarmVC *alarmVC=[[AlarmVC alloc]init];
    FXNavigationController *naviAlarm=[[FXNavigationController alloc]initWithRootViewController:alarmVC];
    
    TodoVC *todoVC=[[TodoVC alloc]init];
    FXNavigationController *naviTodo=[[FXNavigationController alloc]initWithRootViewController:todoVC];
    
    _tabbar=[[UITabBarController alloc]init];
    NSArray *listTabbar=@[naviAlarm,naviHomeVC,naviTodo];
    _tabbar.viewControllers=listTabbar;
    //[ImageSaver saveImageToDisk:[UIImage imageNamed:@"pale.jpg"] andToBeer:paleLager];

//    for (int i = 0; i < 10; i++) {
//                Student *student = (Student *)[NSEntityDescription insertNewObjectForEntityForName:@"Student"
//                                                                            inManagedObjectContext:self.managedObjectContext];
//        
//                student.name    = [NSString stringWithFormat:@"Student %d", i];
//                student.age     = [NSNumber numberWithInt:i+10];
//                student.sex     = [NSNumber numberWithInt:i%2];
//        
//        
//            }
//    
//            [self saveContext];

    return YES;
}
-(void)loginSucess;
{    //HomeVC *homeVC=[[HomeVC alloc]init];

//        UINavigationController *navigationController=[[UINavigationController alloc]initWithRootViewController:homeVC];
    //FXNavigationController *navi=[[FXNavigationController alloc]initWithRootViewController:homeVC];
    self.window.rootViewController=_tabbar;
    //[navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"headline-01.jpg"] forBarMetrics:UIBarMetricsDefault];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Demo0.coredomo" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"coredomo" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"coredomo.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
#pragma ReceiveNotification
-(void)application: (UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state=[application applicationState];
    if (state==UIApplicationStateActive) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Dont waste ur time"
                                                     message:notification.alertBody
                                                    delegate:self
                                           cancelButtonTitle:@"I understand"
                                           otherButtonTitles:nil, nil];
        [alert show];
    }
    //Reload Data
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    //Icon Badge
    application.applicationIconBadgeNumber=0;
}
@end

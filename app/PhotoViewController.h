//
//  PhotoViewController.h
//  coredomo
//
//  Created by LeeQuox on 11/30/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController
@property (weak, nonatomic) UIImage *image;
@end

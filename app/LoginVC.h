//
//  LoginVC.h
//  coredomo
//
//  Created by LeeQuox on 11/29/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *tfUser;
@property (weak, nonatomic) IBOutlet UITextField *tfPass;
@property (weak, nonatomic) IBOutlet UITextField *tfRePass;
@property (weak, nonatomic) IBOutlet UIButton *btLogin;
@property (weak, nonatomic) IBOutlet UIButton *btRegistry;

- (IBAction)btLogin:(id)sender;

- (IBAction)btRegistry:(id)sender;
@end

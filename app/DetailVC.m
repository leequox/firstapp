//change
//  DetailVC.m
//  coredomo
//
//  Created by LeeQuox on 11/29/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "DetailVC.h"
#import "Student.h"
#import "AppDelegate.h"
#import "PhotoViewController.h"
#import "ImageSaver.h"

@interface DetailVC ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,NSFetchedResultsControllerDelegate>
{
    __weak IBOutlet UITextField *_tfAge;
    __weak IBOutlet UITextField *_tfName;
    
    __weak IBOutlet UISegmentedControl *_segmentSex;
    
    __weak IBOutlet UIButton *_btDelete;

}
- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction)deleteSutdent:(id)sender;
@end

@implementation DetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configView];
    // Do any additional setup after loading the view from its nib.
    //self.title=@"Diary";
    if ([self.student.image length] > 0) {
        // Image setup
        NSData *imgData = [NSData dataWithContentsOfFile:[NSHomeDirectory() stringByAppendingPathComponent:self.student.image]];
        [self setImageForStudent:[UIImage imageWithData:imgData]];        //NSLog(@"data%@",[UIImage imageWithData:imgData]);

    }
}
- (void)setImageForStudent:(UIImage*)img {
    
    self.studentImage.image		   = img;
    //self.studentImage.image=image;
    
    //NSLog(@"79%@",img);
    
    self.studentImage.backgroundColor = [UIColor clearColor];
    // self.tapToAddLabel.hidden	   = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void) configView
{
    if (_isEdit) {
        
        //self.title = @"Edit Diary";
        
        _tfName.text                        = _student.name;
        _tfAge.text                         = [NSString stringWithFormat:@"%@",_student.age];
        _segmentSex.selectedSegmentIndex    = [_student.sex integerValue];
        
    }
    //else {self.title = @"Add Diary";}
        

    _btDelete.hidden = !_isEdit;
}

- (IBAction)cancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)done:(id)sender {
    
    if (_tfAge.text && ![_tfAge.text isEqualToString:@""] &&
        _tfName.text && ![_tfName.text isEqualToString:@""] ) {
        
        NSLog(@"Mood: %ld", (long)_segmentSex.selectedSegmentIndex);
        
        AppDelegate *_appDelegate = [UIApplication sharedApplication].delegate;
        
        
        if (_isEdit) {
            NSLog(@"Edit");
            
            _student.name       = _tfName.text;
            _student.age        = [NSNumber numberWithInt:[_tfAge.text intValue]];
            _student.sex        = [NSNumber numberWithInteger:_segmentSex.selectedSegmentIndex];
            _student.date       =_dpDate.date;
           // _student.image      =[]
        } else {
            NSLog(@"Add New");
            
            Student *student    = (Student *)[NSEntityDescription insertNewObjectForEntityForName:@"Student"
                                                                           inManagedObjectContext:_appDelegate.managedObjectContext];
            
            student.name        = _tfName.text;
            student.age         = [NSNumber numberWithInt:[_tfAge.text intValue]];
            student.sex         = [NSNumber numberWithInteger:_segmentSex.selectedSegmentIndex];
            student.date=_dpDate.date;
            //student.image=_studentImage.image;
            
        }
        
        [_appDelegate saveContext];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Input all fields"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        
        [alert show];
    }
    
}

- (IBAction)deleteSutdent:(id)sender {
    AppDelegate *_appDelegate = [UIApplication sharedApplication].delegate;
    
    [_appDelegate.managedObjectContext deleteObject:self.student];
    [_appDelegate saveContext];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btPickImage:(id)sender {
    UIActionSheet *sheet;
    sheet = [[UIActionSheet alloc] initWithTitle:@"Pick Photo"
                                        delegate:self
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:nil
                               otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
    
    [sheet showInView:self.navigationController.view];
}
//- (IBAction)takePicture:(UITapGestureRecognizer*)sender  {
//    UIActionSheet *sheet;
//    sheet = [[UIActionSheet alloc] initWithTitle:@"Pick Photo"
//                                        delegate:self
//                               cancelButtonTitle:@"Cancel"
//                          destructiveButtonTitle:nil
//                               otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
//    
//    [sheet showInView:self.navigationController.view];
//}

//
//- (IBAction)btPickImage:(id)sender {
//    _imagePickerController=[[UIImagePickerController alloc]init];
//    _imagePickerController.delegate=self;
//    _imagePickerController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
//    [self presentModalViewController:_imagePickerController animated:YES];
//}
//
////
//
//-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//{
//    _imgImage.image=[info objectForKey:UIImagePickerControllerOriginalImage];
//    [self dismissModalViewControllerAnimated:YES];
//    
//}
//
//-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
//{
//    
//    
//    
//    [self dismissModalViewControllerAnimated:YES];
//}
//- (IBAction)btPickImage:(UITapGestureRecognizer*)sender {
//    UIActionSheet *sheet;
//    sheet = [[UIActionSheet alloc] initWithTitle:@"Pick Photo"
//                                        delegate:self
//                               cancelButtonTitle:@"Cancel"
//                          destructiveButtonTitle:nil
//                               otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
//    
//    [sheet showInView:self.navigationController.view];
//}
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    PhotoViewController *upcoming = segue.destinationViewController;
//    upcoming.image = self.studentImage.image;
//}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //self.studentImage.image=[UIImage imageNamed:@"chelsea (1).jpg"];
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    //self.studentImage.image=image;

    if (self.student.image) {
        [ImageSaver deleteImageAtPath:self.student.image];
    }
    if ([ImageSaver saveImageToDisk:image andToStudent:self.student]) {
        
        
        
        [self setImageForStudent:image];
        //PhotoViewController *photo=[[PhotoViewController alloc]in];
        //self.studentImage.image=photo.image;
        //self.studentImage.image=image;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    switch (buttonIndex) {
//        case 0: {
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
//                [self presentViewController:imagePicker animated:YES completion:nil];
//            }
//        }
//            break;
        case 1: {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
}





@end

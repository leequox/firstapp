//
//  ImageSaver.m
//  coredomo
//
//  Created by LeeQuox on 11/30/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "ImageSaver.h"
#import "Student.h"


@implementation ImageSaver
+ (BOOL)saveImageToDisk:(UIImage *)image andToStudent:(Student*)student {
    NSData *imgData   = UIImageJPEGRepresentation(image, 0.5);
    NSString *name    = [[NSUUID UUID] UUIDString];
    NSString *path	  = [NSString stringWithFormat:@"Documents/%@.jpg", name];
    NSString *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:path];
    if ([imgData writeToFile:jpgPath atomically:YES]) {
        student.image = path;
        NSLog(@"path: %@",path);
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"There was an error saving your photo. Try again."
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles: nil] show];
        return NO;
    }
    return YES;
}

+ (void)deleteImageAtPath:(NSString *)path {
    NSError *error;
    NSString *imgToRemove = [NSHomeDirectory() stringByAppendingPathComponent:path];
    [[NSFileManager defaultManager] removeItemAtPath:imgToRemove error:&error];
}

@end

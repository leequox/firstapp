//
//  LoginVC.m
//  coredomo
//
//  Created by LeeQuox on 11/29/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "LoginVC.h"
#import "AppDelegate.h"
#import "PhotoViewController.h"
#import "ImageSaver.h"
#import "AnimatedGIFImageSerialization.h"
@interface LoginVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imgTest;

@end

@implementation LoginVC

- (void)viewDidLoad {
    _imgTest.image= [UIImage imageNamed:@"super-qr-code.gif"];
    [super viewDidLoad];
    NSUserDefaults *userdefault=[NSUserDefaults standardUserDefaults];
    if (![userdefault boolForKey:@"registered"]) {
        _btLogin.hidden=YES;
    }
    else{_btRegistry.hidden=YES;
        _tfRePass.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btLogin:(id)sender {
    NSUserDefaults *userdefault=[NSUserDefaults standardUserDefaults];
    
    if ([_tfUser.text isEqualToString:[userdefault objectForKey:@"user"]]&&[_tfPass.text isEqualToString:[userdefault objectForKey:@"password"]]) {
        AppDelegate *appDelegate=[AppDelegate sharedAppDelegate];
        [appDelegate loginSucess];
    }
    else{
        UIAlertView *saimatkau=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Password or User invalid" delegate:self cancelButtonTitle:@"Ok,i will enter again" otherButtonTitles:@"Cancel", nil];
        [saimatkau show];}
}

- (IBAction)btRegistry:(id)sender {
    if ([_tfUser.text isEqualToString:@""]||[_tfPass.text isEqualToString:@""]) {
        UIAlertView *notfill=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please fill in user and password" delegate:self cancelButtonTitle:@"Ok,i will fill" otherButtonTitles:@"Cancel", nil];
        [notfill show];
    }
    else[self checkRePassword];
}
-(void)checkRePassword
{
    if ([_tfPass.text isEqualToString:_tfRePass.text] ){
        [self registryNewUser];
    }
    else{
        UIAlertView *dontmathpass=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Password not math" delegate:self cancelButtonTitle:@"Ok,i will fill again" otherButtonTitles:@"Cancel", nil];
        [dontmathpass show];
        
    }
}
-(void)registryNewUser
{
    NSUserDefaults *userdefault=[NSUserDefaults standardUserDefaults];
    [userdefault setObject:_tfUser.text forKey:@"user"];
    [userdefault setObject:_tfPass.text forKey:@"password"];
    
    [userdefault setBool:YES forKey:@"registered"];
    AppDelegate *appDelegate=[AppDelegate sharedAppDelegate];
    UIAlertView *dkthanhcong=[[UIAlertView alloc]initWithTitle:@"Warning" message:@"Registry finish" delegate:self cancelButtonTitle:@"Ok,i will remember" otherButtonTitles:@"Cancel", nil];
    [dkthanhcong show];
    [appDelegate loginSucess];
}
@end

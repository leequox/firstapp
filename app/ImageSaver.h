//
//  ImageSaver.h
//  coredomo
//
//  Created by LeeQuox on 11/30/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>
#import "Student.h"
#import <UIKit/UIAlertView.h>

@class Student;
@interface ImageSaver : NSObject
+ (BOOL)saveImageToDisk:(UIImage*)image andToStudent:(Student*)student;
+ (void)deleteImageAtPath:(NSString*)path;
@end

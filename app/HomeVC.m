//
//  HomeVC.m
//  coredomo
//
//  Created by LeeQuox on 11/29/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "HomeVC.h"
#import "AppDelegate.h"
#import "DetailVC.h"
#import "Student.h"
@interface HomeVC ()<NSFetchedResultsControllerDelegate>
{

    //__weak IBOutlet UITableView *_tableView;
}

@property(strong,nonatomic)NSFetchedResultsController *fetchStudens;
@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchStudens];
    //self.title=@"My Diary";
    //UIBarButtonItem *add=[[UIBarButtonItem alloc]initWithTitle:@"Add" style:UIBarButtonSystemItemAdd target:self action:@selector(doAdd)];
    //UIBarButtonItem *add=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"plus-25.png"] style:UIBarButtonSystemItemAdd target:self action:@selector(doAdd)];//Title:@"Add" style:UIBarButtonSystemItemAdd target:self
//action:@selector(doAdd)];
    //self.navigationItem.rightBarButtonItem=add;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}
    -(void)doAdd
{
    DetailVC *detailVC=[[DetailVC alloc]init];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadData];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
}

#pragma mark - Fetch Controller

- (NSFetchedResultsController*)fetchStudens
{
    if (!_fetchStudens) {
        NSString *entityName = @"Student";
        AppDelegate *_appDelegate = [UIApplication sharedApplication].delegate;
        
        NSString *cacheName = [NSString stringWithFormat:@"%@",entityName];
        [NSFetchedResultsController deleteCacheWithName:cacheName];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:_appDelegate.managedObjectContext];
        
        
        NSSortDescriptor *sort0 = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        NSArray *sortList = [NSArray arrayWithObjects:sort0, nil];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name != nil"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        fetchRequest.fetchBatchSize = 20;
        fetchRequest.sortDescriptors = sortList;
        fetchRequest.predicate = predicate;
        _fetchStudens = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                            managedObjectContext:_appDelegate.managedObjectContext
                                                              sectionNameKeyPath:nil
                                                                       cacheName:cacheName];
        _fetchStudens.delegate = self;
        
        NSError *error = nil;
        [_fetchStudens performFetch:&error];
        if (error) {
            NSLog(@"%@ core data error: %@", [self class], error.localizedDescription);
        } else {
            NSLog(@"total item : %lu",(unsigned long)[_fetchStudens.fetchedObjects count]);
        }
    }
    
    return _fetchStudens;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_fetchStudens.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    Student *student = _fetchStudens.fetchedObjects[indexPath.row];
    
    cell.textLabel.text = student.name;
    //NSLog(@"%@",student.name);
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Mood : %@ - Date : %@ ", ([student.sex intValue] == 0) ? @"Bored": @"Happy",student.date];
   //Pass image to cell
    //NSString *path=[NSString stringWithFormat:@"%@", student.image];
    //NSArray *array=[path componentsSeparatedByString:@"/"];
    
    //NSLog(@"Name of image :%@",[array objectAtIndex:1]);
    //NSString *nameimage=[NSString stringWithFormat:@"%@",[array objectAtIndex:1]];
    //cell.imageView.image=[UIImage ima];
    //-----------
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailVC *detailVC=[[DetailVC alloc]init];

    //DetailVC *vc                = [storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    
    detailVC.isEdit                   = YES;
    detailVC.student                  = _fetchStudens.fetchedObjects[indexPath.row];
    
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (IBAction)btAdd:(id)sender {
    [self doAdd];
}
//-----
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        Beer *beerToRemove = self.beers[indexPath.row];
//        // Remove Image from local documents
//        if (beerToRemove.beerDetails.image) {
//            [ImageSaver deleteImageAtPath:beerToRemove.beerDetails.image];
//        }
//        // Deleting an Entity with MagicalRecord
//        [beerToRemove deleteEntity];
//        [self saveContext];
//        [self.beers removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    }
//}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	PhotoViewController *upcoming = segue.destinationViewController;
	upcoming.image = self.beerImage.image;
 }
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 PhotoViewController *upcoming = segue.destinationViewController;
	upcoming.image = self.beerImage.image;
}
*/
//-------

//-------
@end

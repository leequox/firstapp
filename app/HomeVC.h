//
//  HomeVC.h
//  coredomo
//
//  Created by LeeQuox on 11/29/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Student.h"
@interface HomeVC : UITableViewController
@property (nonatomic, strong) Student *student;

@end
